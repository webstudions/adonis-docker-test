FROM node:8

ARG SSH_KEY

RUN mkdir -p /root/.ssh && \
    chmod 0700 /root/.ssh && \
    ssh-keyscan bitbucket.org > /root/.ssh/known_hosts && \
    echo "${SSH_KEY}" > /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa

RUN git clone git@bitbucket.org:webstudions/adonis-docker-test.git

WORKDIR /adonis-docker-test

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]
